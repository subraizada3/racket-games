#lang racket

(provide main)
(require picturing-programs)

(define GRIDSIZE 10)
(define TILESIZE 30)
(define PIC:WHITE (square TILESIZE "solid" "white"))
(define PIC:BLACK (square TILESIZE "solid" "black"))
(define PIC:RED   (square TILESIZE "solid" "red"))
(define PIC:BLUE  (square TILESIZE "solid" "blue"))
(define PIC:BG    (square (* GRIDSIZE TILESIZE) "solid" "white"))
(define PIC:WON   (text "Won!" (real->int (* TILESIZE GRIDSIZE 0.2)) "green"))
(define PIC:LOST  (text "Lost" (real->int (* TILESIZE GRIDSIZE 0.2)) "green"))
(define PIC:GREENBAR      (rectangle (* GRIDSIZE TILESIZE) TILESIZE "solid" "green"))
(define PIC:BLACKBAR      (rectangle (* GRIDSIZE TILESIZE) TILESIZE "solid" "black"))
(define PIC:REDBAR        (rectangle (* GRIDSIZE TILESIZE) TILESIZE "solid" "red"))
(define PIC:BLUEBAR       (rectangle (* GRIDSIZE TILESIZE) TILESIZE "solid" "blue"))

(struct client (g c t won) #:transparent)
(define START_STATE (client (make-vector 36) 0 #f 0))

(define (gettilex x) (quotient x TILESIZE))
(define (gettiley y) (quotient y TILESIZE))
(define (getmousepos x y)
  (+ (* GRIDSIZE (gettiley y)) (gettilex x)))

(define (getimgx pos) (* pos TILESIZE))
(define (getimgy pos) (* pos TILESIZE))

(define (onkey s e) (if (key=? e "escape") #f s))

(define (ondraw s)
  (define wonlostpic (ondraw-helper s))
  (cond [(or (equal? #t (client-won s)) (equal? #f (client-won s)))
         (above wonlostpic PIC:GREENBAR PIC:GREENBAR)]
        [(client-t s)
         (above wonlostpic PIC:GREENBAR
                (if (equal? (client-c s) 1)
                    PIC:REDBAR
                    PIC:BLUEBAR))]
        [else (above wonlostpic PIC:GREENBAR PIC:BLACKBAR)]))

(define (ondraw-helper s)
  (define maxcol (sub1 GRIDSIZE))
  (let-values ([(x y z)
                (for/fold ([pic PIC:BG]
                           [col 0]
                           [row 0])
                          ([i (client-g s)])
                  (values
                   (place-image/align (case i
                                        [(0) PIC:WHITE]
                                        [(1) PIC:RED]
                                        [(-1) PIC:BLUE]
                                        [(10) PIC:BLACK])
                                      (getimgx col)
                                      (getimgy row)
                                      "left" "top"
                                      pic)
                   (if (equal? col maxcol) 0 (add1 col))
                   (if (equal? col maxcol) (add1 row) row)))])
    (case (client-won s)
      [(0)  x]
      [(#t) (overlay PIC:WON x)]
      [(#f) (overlay PIC:LOST x)])))

(define (onrec s m)
  (cond [(equal? m 'red)  (struct-copy client s [c 1])]
        [(equal? m 'blue) (struct-copy client s [c -1])]
        [(equal? m 'go)   (struct-copy client s [t #t])]
        [(equal? m 'won)  (struct-copy client s [won #t])]
        [(equal? m 'lost) (struct-copy client s [won #f])]
        [(list? m)        (struct-copy client s [g (list->vector m)])]
        [(equal? m 'quit) #f]
        [else s]))

(define (onmouse s x y e)
  (if (and (client-t s)
           (string=? e "button-down")
           (< y (* GRIDSIZE TILESIZE)))
      (onmouse-helper s (getmousepos x y))
      s))

(define (onmouse-helper s pos)
  (define v (client-g s))
  (define newgrid
    (vector-append (vector-copy v 0 pos)
                   (make-vector 1 (client-c s))
                   (vector-copy v (add1 pos))))
  (make-package (struct-copy client s
                             [t #f])
                (vector->list newgrid)))

(define (main n i s)
  (big-bang START_STATE
            (on-key onkey)
            (on-mouse onmouse)
            (on-draw ondraw (* GRIDSIZE TILESIZE) (* (+ GRIDSIZE 2) TILESIZE))
            (stop-when boolean?)
            (close-on-stop #t)
            (state s)
            (on-receive onrec)
            (register i)
            (name n)))
