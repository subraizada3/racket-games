# Collection of (mostly) games
With the exception of checkers and spacewar (local multiplayer) and set (single-player), all of these are client/server networked applications.

In general, the server auto-starts the clients on the same computer, but they have been tested to run at least over LAN, with the clients manually started on another computer.

For some reason, a few of these only work properly when started from the DrRacket IDE - running `racket server.rkt` from a terminal doesn't start the server/clients. Thus, the preferred way to run these is to open the server script in DrRacket and run it from there.

With most of these, running the client on a separate machine is accomplished by

1. Removing the client autostart from the bottom of the server file, and
2. Adding a call to the (main) or (run) method (with the same arguments as from the server autostart, and the correct IP address) onto the bottom of the client file, and then executing the client file.

# Game descriptions
## [Tic-Tac-Toe](ttt)
Tic-Tac-Toe, human vs human or human vs Minimax.

## [Life](life)
A game where two players try to 'colonize' a game of life board by taking turns flipping one square to their color (between iterations of the game of life). Can be played 2-player or against a (slow) Minimax AI. This game is way too complex for humans to be able to comprehend...

## [Chat](chat)
Not quite a game... A client/server chat application supporting an arbitrary number of users. The server auto-starts three clients.

## [Spacewar!](spacewar)
Non-networked clone of [Spacewar!](https://en.wikipedia.org/wiki/Spacewar!). Later [rewritten in Java](https://bitbucket.org/subraizada3/spacewar).

## [Car](car)
-Multiplayer game set on a 5-lane road, where one player sends vehicles down the lanes, and the other has to dodge them.

## [Checkers](checkers)
Non-networked implementation of checkers - run `checkers2.rkt`.

## [Pong](pong)
Pong... what more needs to be said?

## [Set](set)
Implementation of the single-player game [Set](https://en.wikipedia.org/wiki/Set_(game))

# Gameplay instructions
## [Tic-Tac-Toe](ttt)
Turn the AI on or off, and set whether it goes on the first or second turn, with the options on lines 3 and 4 (`AI` and `AIGOESFIRST`).

Your team (X or O) is indicated at the bottom of the window. A green line means it's your turn. A full green/red grid indicates win/loss. Place your piece with the numbers 1-9 - the spot chosen is the number's location on a normal computer numpad.

## [Life](life)
The `AI` variable on line 3 determines wheter an AI will be used for the second player or not.

The game uses the standard game of life (B3/S23) rules. A player's color is indicated by the color of the bottom of their game window (which is black when it's not their turn). Click a tile to convert it to your color. Then, the game will iterate one step and let the other player move. The number of neighbors a tile has is not affected by the color of its neighbors (2 blue tiles can keep a red tile alive). A player wins when his color is present on at least 25% of the grid.

The AI is quite slow (brute force minimax with limited depth), so the best way to just try the game is to play against yourself. It doesn't really matter whether you're playing against an AI or yourself, because the game is just too complex - we aren't capable of thinking about what all the effects of a move will be on a 10x10 grid and then taking into account future actions by oneself or the enemy.

## [Chat](chat)
Unless server autostart is used, IP and username is prompted for when the client is started. Type a message and press enter to send it.

## [Spacewar!](spacewar)
Needle - Player 1 (indicators on left side): turn with <kbd>A</kbd> and <kbd>D</kbd>, fire with <kbd>W</kbd>, thrust with <kbd>S</kbd>, hyperspace with <kbd>space</kbd>.

Wedge - Player 2 (indicators on right side): turn with <kbd>←</kbd> and <kbd>→</kbd>, fire with <kbd>↑</kbd>, thrust with <kbd>↓</kbd>, hyperspace with <kbd>0</kbd> (top row or numpad).

Hyperspace is still new, unreliable technology, and the chance of accidentally exploding increases every time it is used.

## [Car](car)
The energy of the aggressor (triangle) fills at an inreasing rate, up to a maximum of 8. The left and right arrows move both players. For the aggressor, the numbers 1-3 are used to send vehicles down the road. <kbd>1</kbd> sends a 2-energy 1x1 car. <kbd>2</kbd> sends a 3-energy 1x3 (long) truck. <kbd>3</kbd> sends a 7-energy 3x2 (wide) boss - due to its width, it cannot be sent while on the first or last lane. Players switch sides when the aggressor wins.

## [Checkers](checkers)
Move the cursor with arrow keys, space to select or deselect a piece, and left/right arrow to move.

## [Pong](pong)
Up and down arrow keys to move the paddle.

## [Set](set)
[Game rules](https://en.wikipedia.org/wiki/Set_(game)). Click tiles to add them to the current selection. Clicking again after selecting three tiles will submit the selection. Sets that are valid get added below the current selection display area, and invalid sets are discarded.
