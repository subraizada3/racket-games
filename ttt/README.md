# Tic-Tac-Toe
### 2-player or 1-player vs Minimax AI
I made this long enough ago that I don't remember all the configuration options and code. However, lines 3 and 4 of `ttt_server.rkt` (AI and AIGOESFIRST) control whether the game is 2 player or vs AI, and whether the AI will go first or second.

While it is client/server networked, the server is responsible for starting the clients, and so the clients are always run on the same machine as the server and connect to localhost. This can be changed by removing the client autostart from the bottom of `ttt_server.rkt`, and calling the `main` method in `ttt_client.rkt` on the client computer, with the following arguments:

* "1" or "2": the player number
* The IP address to connect to
* \#f

\#f is the initial state of the client. I can't recall what exactly it represents.
